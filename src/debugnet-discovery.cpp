/*
 * debugnet-discovery.cpp
 *
 *  Created on: 8 Feb 2019
 *      Author: sgasper
 */

#include "debugnet-discovery.h"

/*
 * Add the type of connection of every discovered device.
 */
vector<Device> addConnectionTypes(vector<Device> discoveredDevices,
		DiscoveryConfiguration conf){
	regex macPattern("[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}");
	vector<Device> processedDevices;

	// Set the NIC name associated with each WiFI based connection type. We want
	// to distinguish which devices are connected using the 2.4ghz spectrum and
	// which use the 5ghz spectrum, a nic is associated with each spectrum.
	vector<tuple<CONNECTION_TYPE, string>> wifiNics {
		make_tuple(CONN_WIFI_2_4, conf.nic2GhzWiFi),
		make_tuple(CONN_WIFI_5, conf.nic5GhzWifi),
	};

	// Iterate over each WiFi connection type and determine which of the
	// discovered devices are connected to the network using that type
	for (tuple<CONNECTION_TYPE, string> wifiNic : wifiNics){
		CONNECTION_TYPE connType = get<0>(wifiNic);
		string nicName = get<1>(wifiNic);

		// Use 'iw' to see which WiFi devices are using the wireless enabled NIC
		const string stationInfo = execIwStationDumpForNic(nicName);

		// Slice the command output to pick out every mac address referenced.
		for(sregex_iterator i = sregex_iterator(stationInfo.begin(),
												stationInfo.end(),
												macPattern);
					i != std::sregex_iterator(); ++i ){
			// Extract mac address string and ensure all its characters are
			// upper case.
			smatch match = *i;
			string foundMac = match.str();
			for (auto& c: foundMac) c = toupper(c);

			// Go through each discovered device to locate the device with the
			// output mac address
			for (unsigned int di=0; di < discoveredDevices.size(); di++){
				Device device = discoveredDevices[di];

				if (! foundMac.compare(device.getMacAddress())){
					// Device found with same mac, set the connection type, mark
					// it as processed and remove it from the original vector
					// as we wont need to loop over it again.
					device.setConnectionType(connType);
					processedDevices.push_back(device);
					discoveredDevices.erase(discoveredDevices.begin() + di);
					break;
				}
			}
			// Do nothing if no discovered device matches the mac address as
			// a likely reason for this is because 'iw' was executed after
			// 'nmap' so their is a chance some of the network state could of
			// changed in the mean time.
		}
    }

	// Mark the connection type as other for every discovered device that had
	// not already yet been set a connection type.
	for (Device device : discoveredDevices){
		device.setConnectionType(CONN_OTHER);
		processedDevices.push_back(device);
	}

	return processedDevices;
}

/*
 * Check and apply differences for a device discovered device against what has
 * previously been known about it in the database.
 */
void applyUpdatesFromDiscoveryToDevice(Device discDevice, Device& toDevice){
	// If the type of device connection method has changed then update it
	if (discDevice.getConnectionType() != toDevice.getConnectionType()){
		toDevice.setConnectionType(discDevice.getConnectionType());
	}

	// Set the device as online if previously it was offline
	if (discDevice.getStatus() != toDevice.getStatus()){
		toDevice.setStatus(discDevice.getStatus());
	}

	// Update IP address if it has changed
	if (discDevice.getIpAddress().compare(toDevice.getIpAddress())){
		toDevice.setIpAddress(discDevice.getIpAddress());
	}

	// Update hostname if it has changed
	if (discDevice.getHostname().compare(toDevice.getHostname())){
		toDevice.setHostname(discDevice.getHostname());
	}
}

/*
 * Execute the 'iw' command line tool to get information about every wireless
 * device connected to the specified (wireless enabled) NIC.
 */
string execIwStationDumpForNic(string nicName){
	string iwCmd = "iw " + nicName + " station dump";
	CommandResult iwResult = executeCmdOnRouter(iwCmd);

	// Throw an error if their was an issue executing the 'iw' command
	if (iwResult.exitCode != 0){
		throw CMDExecutionOnRouterException(iwResult.cmd, iwResult.out);
	}

	return iwResult.out;
}

/*
 * Execute the 'nmap' command line tool to scan network for online devices
 */
void execNmapScan(string nmapResultsPath, string ipScanRange, int hostTimeout){
	// Build nmap command line. Args:
	//		-R  			-> always do DNS lookup
	//		-sn 			-> do ping scan and disable port scan
	//		--host-timeout	-> time to give up waiting for host response (seconds)
	//		-Ox [FILE]		-> XML file to output scan results to
	string nmapCmd = "nmap  -R -sn --host-timeout " + to_string(hostTimeout) + \
			"s -oX " + nmapResultsPath + " " + ipScanRange;

	// Execute nmap
	CommandResult nmapResult = executeCmdOnRouter(nmapCmd);

	// Throw an error if their was an issue executing the 'nmap' command
	if (nmapResult.exitCode != 0){
		throw CMDExecutionOnRouterException(nmapResult.cmd, nmapResult.out);
	}
}

/*
 * Retrieve the latest configuration from the database
 */
DiscoveryConfiguration getDiscoveryConfig(){
	DiscoveryConfiguration conf;

	conf.ipScanRange = getConfig("ip_scan_range").getValue<string>();
	conf.hostResponseToNmapTimeout = getConfig(
			"host_response_to_nmap_timeout").getValue<int>();
	conf.nmapResultsFilePath = getConfig("nmap_results_file_path").getValue<string>();
	conf.waitBetweenScans = getConfig("discovery_wait_between_scans").getValue<int>();
	conf.nic2GhzWiFi = getConfig("nic_name_wifi_2_4_ghz").getValue<string>();
	conf.nic5GhzWifi = getConfig("nic_name_wifi_5_ghz").getValue<string>();

	return conf;
}

/*
 * Processes discovered devices against all devices stored in the DB. The
 * function works out which devices are new and which have already been
 * discovered. The devices that have already previously been discovered have
 * their information updated against the current state of the device as
 * identified by the discovery process.
 */
void processDiscoveredDevicesAgainstDb(vector<Device> discoveredDevices,
		vector<Device>& newDevices, vector<Device>& updatedDevices){
	vector<Device> allDbDevices = getAllDevices();

	for (Device discDevice : discoveredDevices){
		bool newDevice = true;

		// Check if the device has already been discovered or if its a new
		// device.
		for (unsigned int i = 0; i < allDbDevices.size(); i++){
			if (! discDevice.getMacAddress().compare(allDbDevices[i].getMacAddress())){
				// Update the time the device was last seen
				allDbDevices[i].setLastConnectedTime(discDevice.getLastConnectedTime());
				// Update details of device with the information collected from
				// the latest discovery.
				applyUpdatesFromDiscoveryToDevice(discDevice, allDbDevices[i]);
				// Mark the device as updated
				updatedDevices.push_back(allDbDevices[i]);
				newDevice = false;
				// Remove found device from vector so we know which devices
				// stored in the DB have not been discovered this time round.
				allDbDevices.erase(allDbDevices.begin() + i);
				break;
			}
		}

		// Mark device as new if it cannot be found in the DB
		if (newDevice == true){
			newDevices.push_back(discDevice);
		}
	}

	// Update attributes of all devices stored in the DB but have not been
	// discovered this time round.
	for (Device device : allDbDevices){
		// Device is no longer online
		device.setStatus(DEV_OFFLINE);
		// As the the device is offline it will not have a connection type
		device.setConnectionType(CONN_NONE);
		// As the the device is offline it will not have a IP address
		device.clearIpAddress();

		updatedDevices.push_back(device);
	}
}

/*
 * Scan the network to determine online devices
 */
vector<Device> scanNetworkForOnlineDevices(DiscoveryConfiguration conf){
	vector<Device> discoveredDevices;
	xml_document doc;

	// Use 'nmap' to scan network for online devices
	execNmapScan(conf.nmapResultsFilePath, conf.ipScanRange,
			conf.hostResponseToNmapTimeout);

	time_t execTime = time(0);


	// Load nmap xml output into an xml parser
	if (!doc.load_file(conf.nmapResultsFilePath.c_str())){
		throw FileNotFoundException("nmap XML output file",
				conf.nmapResultsFilePath);
	}

    for (xml_node host = doc.child("nmaprun").child("host"); host;
    		host = host.next_sibling("host")){
    	string hostname;
    	string ipAddress;
    	string macAddress;
    	string vendor;

    	for (xml_node addr = host.child("address"); addr; addr = addr.next_sibling("address")){
    		if (! string("ipv4").compare(addr.attribute("addrtype").value())){
    			ipAddress = addr.attribute("addr").value();
    		}
    		else if (! string("mac").compare(addr.attribute("addrtype").value())){
    			macAddress = addr.attribute("addr").value();
    			vendor = addr.attribute("vendor").value();
    		}
    	}

    	// Don't both with devices who's MAC addresses can't be detected as
    	// debugnet requires this parameter to uniquely identify a device in
    	// the network.
    	if (macAddress == ""){
    		continue;
    	}

    	// Multiple hostnames can be reported but we only care for one, so we
    	// take the first.
    	xml_node hostNames = host.child("hostnames");
    	hostname = hostNames.child("hostname").attribute("name").value();

    	Device discoveredDevice(macAddress, ipAddress, execTime, vendor);

    	// Set the hostname of the device if one was detected
    	if (hostname != ""){
    		discoveredDevice.setHostname(hostname);
    	}

    	discoveredDevices.push_back(discoveredDevice);
    }

    return addConnectionTypes(discoveredDevices, conf);
}

int main() {
	DiscoveryConfiguration conf = getDiscoveryConfig();

	// Enter continuous loop of network discovery
	while (true){
		vector<Device> newDevices;
		vector<Device> updatedDevices;

		// Scan network for online devices
		vector<Device> discoveredDevices = scanNetworkForOnlineDevices(conf);

		// Process new devices and update previously known devices
		processDiscoveredDevicesAgainstDb(discoveredDevices, newDevices,
				updatedDevices);

		// Display discovered devices
		cout << "Discovered devices (total=" << discoveredDevices.size() << \
				", new=" << newDevices.size() << ")..." << endl;

		for (Device device : discoveredDevices){
			cout << "\nhostname: " + device.getHostname() << endl;
			cout << "ip address: " + device.getIpAddress() << endl;
			cout << "mac: " + device.getMacAddress() << endl;
			cout << "vendor: " + device.getManufacturer() << endl;
			cout << "connection type: " << connTypeEnumToIntId(device.getConnectionType()) << endl;
			cout << "device status: " << deviceStatusEnumToIntId(device.getStatus()) << endl;
		}

		// Update known devices in DB with the latest information
		updateDevices(updatedDevices);

		// Add newly discovered devices to the DB
		addDevices(newDevices);

		// Reset vectors for next cycle
		newDevices.clear();
		updatedDevices.clear();
		discoveredDevices.clear();

		// Separate results from scan 'session'
		cout << string(40, '-') << endl;

		cout << "waiting till next scan..." << endl;

		// Wait some time before the next scan
		this_thread::sleep_for(chrono::seconds(conf.waitBetweenScans));

	}
}


