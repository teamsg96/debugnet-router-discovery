# Third Party Source Code
All source code located in this directory (and sub directories) has been
produced by a third party group, not me.

### Missing files
The packages in this directory contain only the files that are required
to build the project as well as any useful instructions and licensing information. Any unnecessary supplementary files have been discarded.
Please be aware of this when reading any instruction files or comments that
reference content that cannot be seen.