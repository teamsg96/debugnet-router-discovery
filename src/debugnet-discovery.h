/*
 * debugnet-discovery.h
 *
 *  Created on: 8 Feb 2019
 *      Author: sgasper
 */

#ifndef DEBUGNET_DISCOVERY_H_
#define DEBUGNET_DISCOVERY_H_


#include <iostream>
#include <thread>
#include <vector>
#include "third_party_src/pugixml/pugixml.hpp"

#include "debugnet.h"

using namespace pugi;
using namespace std;

/*
 * Holds configuration for discovery operations
 */
struct DiscoveryConfiguration
{
	string ipScanRange;
	int hostResponseToNmapTimeout;
	string nmapResultsFilePath;
	int waitBetweenScans;
	string nic2GhzWiFi;
	string nic5GhzWifi;
};

vector<Device> addConnectionTypes(vector<Device> discoveredDevices,
		DiscoveryConfiguration conf);
void applyUpdatesFromDiscoveryToDevice(Device discDevice, Device& toDevice);
string execIwStationDumpForNic(string nicName);
void execNmapScan(string nmapResultsPath, string ipScanRange, int hostTimeout);
DiscoveryConfiguration getDiscoveryConfig();
void processDiscoveredDevicesAgainstDb(vector<Device> discoveredDevices,
		vector<Device>& newDevices, vector<Device>& updatedDevices);
vector<Device> scanNetworkForOnlineDevices(DiscoveryConfiguration conf);

#endif /* DEBUGNET_DISCOVERY_H_ */
